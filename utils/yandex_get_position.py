import requests
from xml.dom.minidom import parseString
from config.settings import (
    YANDEX_API_URL,
    YANDEX_USER,
    YANDEX_USER_KEY,
)


class ParserYandexXml:

    def get_site_position_and_url_by_keyword(self, params_for_yandex_api: dict, site_domain: str) -> tuple:
        url_tag_values = self.get_response_from_yandex_api(params_for_yandex_api)
        return self.url_tag_parser(site_domain, url_tag_values)

    def get_response_from_yandex_api(self, params_for_yandex_api: dict) -> list:
        response_from_yandex_api = requests.get(YANDEX_API_URL, params=params_for_yandex_api).text
        dom_xml = parseString(response_from_yandex_api)
        error_tag = dom_xml.getElementsByTagName('error')[0] if dom_xml.getElementsByTagName('error') else None

        if error_tag is not None and error_tag.getAttribute("code") == '55':
            return self.get_response_from_yandex_api(params_for_yandex_api)

        return self.get_url_tag_values(dom_xml)

    @staticmethod
    def get_params_for_yandex_api(query: str, region: int, page: int, **kwargs) -> dict:
        """
        all parameters required to work with Yandex api
        query: str, region: int, page: int, language=None, sortby=None, filter=None, groupby=None
        """

        return {
            'user': YANDEX_USER,
            'key': YANDEX_USER_KEY,
            'query': query,
            'lr': region,
            'page': page,
            **kwargs,
        }

    @staticmethod
    def get_url_tag_values(dom_xml) -> list:
        list_tag_values = []
        url_tags = dom_xml.getElementsByTagName('url')

        for tag in url_tags:
            list_tag_values.append(tag.childNodes[0].nodeValue)
        return list_tag_values

    @staticmethod
    def url_tag_parser(site_domain: str, url_tag_values: list) -> tuple:
        position_url_site = tuple()
        for position, url_site in enumerate(url_tag_values, start=1):
            if site_domain in url_site.lower():
                position_url_site += position, url_site

                return position_url_site

        if not position_url_site:
            return 0, site_domain




# parser_yandex_xml = ParserYandexXml()
# params_for_yandex_api = parser_yandex_xml.get_params_for_yandex_api(
#     # query='создать сайт челябинск',  # Прояснить что с этим запросом???
#     query='создание дизайна сайта',
#
#     region=56,
#     page=0,
#     groupby='attr=d.mode=deep.groups-on-page=100.docs-in-group=1'
# )
#
# response_from_yandex_api = parser_yandex_xml.get_site_position_and_url_by_keyword(params_for_yandex_api,
#                                                                                   'https://flexites.org/')
#
# print(response_from_yandex_api)
