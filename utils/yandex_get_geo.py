import requests
import json


class LocationFromYandexAPI:

    def get_matching_cities(self, params_for_request: dict):
        list_regions = self.get_response_from_yandex(params_for_request)['results']
        complete_list_cities = [self.split_region_into_components(region) for region in list_regions]
        return json.dumps(complete_list_cities)

    @staticmethod
    def split_region_into_components(region: dict) -> dict:
        return {'title': region['title'], 'subtitle': region['subtitle'], 'region': region['geoid']}

    @staticmethod
    def get_response_from_yandex(params_for_request: dict, url='http://suggest-maps.yandex.ru/suggest-geo?'):
        response_from_yandex = requests.get(url, params=params_for_request).json()
        return response_from_yandex if response_from_yandex is not None else None

    @staticmethod
    def get_params_for_request(city: str, country='') -> dict:
        params_for_request = {
            'search_type': 'tune', 'v': '9', 'results': '20', 'lang': 'ru', 'part': f'{country} {city}',
        }
        return params_for_request
