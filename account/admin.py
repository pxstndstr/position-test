from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.models import Group

from account.models import CustomUser


class AccountCreationForm(forms.ModelForm):
    """
    Формы для создания новых пользователей.
    """
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput)

    class Meta:
        model = CustomUser
        fields = ('email',)

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class AccountChangeForm(forms.ModelForm):
    """
    Форма для обновления пользователей.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = CustomUser
        fields = ('email', 'password', 'date_joined', 'is_active',)

    def clean_password(self):
        return self.initial['password']


class AccountAdmin(BaseUserAdmin):
    """
    Форма для добавления и изменения пользователей.
    """
    form = AccountChangeForm
    add_form = AccountCreationForm

    list_display = ('id', 'email', 'date_joined',)
    list_filter = ('date_joined',)
    list_display_links = ('email',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Персональная информация', {'fields': ('date_joined',)}),
        ('Права пользователя', {'fields': ('is_active',)}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password',)}
         ),
    )
    list_per_page = 10
    show_full_result_count = False
    date_hierarchy = 'date_joined'
    search_fields = ('email',)
    ordering = ('-id',)
    filter_horizontal = ()


admin.site.register(CustomUser, AccountAdmin)
admin.site.unregister(Group)
