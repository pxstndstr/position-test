from django.urls import path

from account.views import (
    IndexView,
    AccountLoginView,
    AccountLogoutView,
    AccountPasswordChangeView,
    RegisterAccountView,
    RegisterDoneView,
)

app_name = 'account'
urlpatterns = [
    path('password/change/', AccountPasswordChangeView.as_view(), name='password_change'),
    path('register/done', RegisterDoneView.as_view(), name='register_done'),
    path('register/', RegisterAccountView.as_view(), name='register'),
    path('login/', AccountLoginView.as_view(), name='login'),
    path('logout/', AccountLogoutView.as_view(), name='logout'),
    path('', IndexView.as_view(), name='index'),
]
