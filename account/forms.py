from django import forms

from account.models import CustomUser


class RegisterAccountForm(forms.ModelForm):
    email = forms.EmailField(
        required=True,
        label='Адрес электронной почты'
    )
    password = forms.CharField(
        label='Пароль',
        widget=forms.PasswordInput
    )

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user

    class Meta:
        model = CustomUser
        fields = ('email', 'password')
