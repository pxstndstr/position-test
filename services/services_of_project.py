import json

from django.db.models import F, Q
from django.utils import timezone
from collections import namedtuple

from utils.yandex_get_geo import LocationFromYandexAPI
from utils.yandex_get_position import ParserYandexXml
from project.models import (
    Keywords,
    KeywordsGroup,
    Project,
    RankingsSite,
    SearchSystem,
)

SiteRatings = namedtuple('SiteRatings',
                         ['keywords_group_id', 'keyword', 'position', 'url_site', 'search_system_id', 'project_id'])

SearchSystems = namedtuple('SearchSystems', ['name', 'country', 'city', 'geo_id_city', 'language'])


def service_adding_keywords_to_keyword_group(request):
    data_of_keywords_and_keyword_groups = json.loads(request.body)
    keywords_group_id = data_of_keywords_and_keyword_groups['keywords_group']
    keywords = data_of_keywords_and_keyword_groups['addedKeywords']
    Keywords.objects.create(name=keywords, keywords_group_id=keywords_group_id)


def service_adding_keyword_groups(request, project_id: int) -> dict:
    keywords_group_data = json.loads(request.body)
    keywords_group = keywords_group_data['InputOfKeywordsGroup']
    keywords_group_id = KeywordsGroup.objects.create(name=keywords_group, project_id=project_id).id
    return {'keywords_group_id': keywords_group_id}


class SiteRatingsService:

    def get_site_ratings(self, project_id: int) -> list:
        project_data = self.get_project_data_for_request_to_yandex_api(project_id)
        ready_data_of_last_modified_project = self.split_set_of_keywords(project_data)
        groupby = self.get_groupby()

        return [_ for keyword_group_data in ready_data_of_last_modified_project for _ in
                self.get_results_of_yandex_xml_parser(groupby, keyword_group_data)]

    @staticmethod
    def get_project_data_for_request_to_yandex_api(project_id: int) -> list:
        project_data = Project.objects \
            .prefetch_related('searchsystem_set') \
            .values(project_id=F('id'),
                    project_domain=F('domain'),
                    search_system_id=F('searchsystem__id'),
                    keywords_group_id=F('searchsystem__keywordsgroup__id'),
                    geo_id_city=F('searchsystem__geo_id_city'),
                    keywords=F('searchsystem__keywordsgroup__keywords__name')).all() \
            .filter(id=project_id)
        print(project_data)
        return project_data

    @staticmethod
    def get_groupby(attr='d', mode='deep', groups_on_page=100, docs_in_group=1) -> str:
        return f'attr={attr}.mode={mode}.groups-on-page={groups_on_page}.docs-in-group={docs_in_group}'

    @staticmethod
    def get_results_of_yandex_xml_parser(groupby: str, ready_project_data: dict, page=0) -> list:
        site_ratings = []
        parser_yandex_xml = ParserYandexXml()

        for keyword in ready_project_data['keywords']:
            params_for_yandex_api = parser_yandex_xml.get_params_for_yandex_api(
                query=keyword,
                region=ready_project_data['geo_id_city'],
                page=page,
                groupby=groupby
            )
            print(ready_project_data['project_domain'])

            position, url_site = parser_yandex_xml.get_site_position_and_url_by_keyword(
                params_for_yandex_api,
                ready_project_data['project_domain']
            )
            site_ranking_by_keyword = SiteRatings(
                keywords_group_id=ready_project_data['keywords_group_id'],
                project_id=ready_project_data['project_id'],
                search_system_id=ready_project_data['search_system_id'],
                keyword=keyword,
                position=position,
                url_site=url_site,
            )

            site_ratings.append(site_ranking_by_keyword)

        return site_ratings

    @staticmethod
    def split_set_of_keywords(project_data: list) -> list:
        for keyword_group_data in project_data:
            keyword_group_data['keywords'] = keyword_group_data['keywords'].split('\r\n')
            print(keyword_group_data['keywords'])
        return project_data


class SiteRaitingsRepository:

    @staticmethod
    def update_or_create_keyword_rankings(data_with_keyword_ratings):
        for _ in data_with_keyword_ratings:
            RankingsSite.objects.create(keywords=_.keyword,
                                        keywords_group_id=_.keywords_group_id,
                                        search_system_id=_.search_system_id,
                                        project_id=_.project_id,
                                        position=_.position,
                                        url_site=_.url_site, )


class ProjectService:

    def start_checking_projects(self):
        today = timezone.now()
        projects = Project.objects.filter(
            Q(date_of_check__month=today.month) &
            Q(date_of_check__day__lte=today.day) |
            Q(date_of_check=None)
        )



        for project in projects:
            active_projects = SearchSystem.objects.filter(project__slug=project.slug)
            print(active_projects)
            if active_projects:
                if not project.date_of_check:
                    self.get_site_rating_and_load(project)
                    self.update_check_date(project)

                elif project.date_of_check.day <= today.day:
                    self.get_site_rating_and_load(project)
                    self.update_check_date(project)

    @staticmethod
    def get_site_rating_and_load(project: Project):
        site_ratings_service = SiteRatingsService()
        site_ratings = site_ratings_service.get_site_ratings(project.id)
        site_raitings_repository = SiteRaitingsRepository()
        site_raitings_repository.update_or_create_keyword_rankings(site_ratings)

    @staticmethod
    def update_check_date(project: Project):
        date = project.created
        if project.date_of_check:
            date = project.date_of_check
        project.date_of_check = Project.get_next_check_date(project, date)
        project.save()


class SearchSystemService:

    def add_search_system(self, request, project_id: int):
        search_system_data = json.loads(request.body)
        form_field_values = self.get_form_field_values(search_system_data)
        new_search_system = self.create_search_system(form_field_values, project_id)
        self.add_search_system_to_keyword_group(project_id, new_search_system)

    @staticmethod
    def get_cities_to_choose(request) -> str:
        entered_city_data = json.loads(request.body)
        location_from_yandex = LocationFromYandexAPI()
        selected_country = entered_city_data['country']
        selected_city = entered_city_data['select_city']
        if selected_city:
            params_for_request = location_from_yandex.get_params_for_request(selected_city, selected_country)
            return location_from_yandex.get_matching_cities(params_for_request)

    @staticmethod
    def get_form_field_values(search_system_data) -> SearchSystems:
        name = search_system_data['name']
        country = search_system_data['country']
        city = search_system_data['select_city']
        geo_id_city = search_system_data['selected_city_id']
        language = search_system_data['language']

        return SearchSystems(name=name, country=country, city=city, geo_id_city=geo_id_city, language=language)

    @staticmethod
    def add_search_system_to_keyword_group(project_id: int, new_search_system: SearchSystem):
        keywords_groups = KeywordsGroup.objects.filter(project_id=project_id)
        for keywords_group in keywords_groups:
            keywords_group.search_systems.add(new_search_system)

    @staticmethod
    def create_search_system(form_field_values: SearchSystems, project_id: int) -> SearchSystem:
        new_search_system = SearchSystem.objects.create(
            name=form_field_values.name,
            country=form_field_values.country,
            city=form_field_values.city,
            geo_id_city=form_field_values.geo_id_city,
            language=form_field_values.language,
            project_id=project_id
        )
        return new_search_system
