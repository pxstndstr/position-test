const requestHeaders = new Headers();

export async function sendRequest(request_options) {
    try {
        let init = {};

        if (request_options.method === 'POST') {
            Object.entries(request_options.headers).forEach(([key, value]) => {
                requestHeaders.set(key, value);
            });

            init = {
                method: request_options.method,
                body: request_options.body,
                headers: requestHeaders,
            };
        }

        const request = new Request(request_options.url, init);

        return await fetch(request)
            .then(response => response.json());

    } catch (err) {
        console.log(err);
        return await Promise.reject(err)
    }
}