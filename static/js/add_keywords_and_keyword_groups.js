import {sendRequest} from "./send-request.js";
import {getFormValues} from './get-form-values.js'
import {getCookie} from './get-cookie.js'
import {CreateObject, CreateForm} from './create-object-or-element-or-form.js'


// При активации CSRF_USE_SESSIONS или CSRF_COOKIE_HTTPONLY csrftoken нужно
// получать другим способом см. документацию:
const csrftoken = getCookie('csrftoken');
let blockForGeneratedForm = document.getElementById('idCreateKeywordGroup');
let formForCreatingKeywords = document.getElementById('formIdForCreateKeywords');


let objectIsInput = new CreateObject({
        element: 'input',
        id: 'idInputOfKeywordsGroup',
        name: 'InputOfKeywordsGroup',
        class: 'form-control',
        type: 'text',
        required: true,
    }
);

let objectIsButton = new CreateObject({
        element: 'button',
        id: 'idSubmitButton',
        class: 'btn btn-success',
        type: 'submit',
    }
);

let objectIsLabel = new CreateObject({
        element: 'label',
        for: 'idInputOfKeywordsGroup',
    }
);

let objectIsForm = new CreateObject({
    element: 'form',
    method: 'POST',
    id: 'idFormCreatingGroupOfKeywords',
    class: 'form-control',
});


export let addedForm = function addForm() {
    let valueOfSelectedOption = this.options[this.selectedIndex].value;
    let requestURL = blockForGeneratedForm.dataset['url'];

    if (valueOfSelectedOption === "-1") {
        let formCreatingGroupsOfKeywords = new CreateForm(
            objectIsForm,
            objectIsInput,
            objectIsButton,
            objectIsLabel
        );

        blockForGeneratedForm.innerHTML = '';
        blockForGeneratedForm.appendChild(formCreatingGroupsOfKeywords);

        formCreatingGroupsOfKeywords.addEventListener('submit', (e) => {
            e.preventDefault();

            let formValues = getFormValues(formCreatingGroupsOfKeywords);
            const requestOptions = new CreateObject({
                method: 'POST',
                url: requestURL,
                headers: {
                    'X-CSRFToken': csrftoken,
                    'Content-type': 'application/json',
                },
                body: JSON.stringify(formValues),
            });

            sendRequest(requestOptions)
                .then((keywordsGroupId) => {
                    let keywordGroupName = formValues['InputOfKeywordsGroup'];
                    let option = new Option(keywordGroupName, keywordsGroupId['keywords_group_id']);
                    let optionToCreateGroup = document.getElementById('createGroup');
                    let select = document.getElementById('idSelectKeywordsGroup');
                    select.insertBefore(option, optionToCreateGroup);

                })
                .catch(err => console.log(err));
            // Удаляем созданную форму после отправки данных на сервер.
            blockForGeneratedForm.removeChild(formCreatingGroupsOfKeywords)

        });
    } else {

        let formCreatingGroupsOfKeywords = document.getElementById('idFormCreatingGroupOfKeywords');

        // Удаляем форму для создания группы ключевых слов, в случае выбора в поле для
        // выбора групп ключевых слов конкретной группы, а не пункта "создать группу".
        if (formCreatingGroupsOfKeywords) {
            blockForGeneratedForm.removeChild(formCreatingGroupsOfKeywords)
        }
    }
};


if (formForCreatingKeywords) {
    formForCreatingKeywords.addEventListener('submit', (e) => {
        e.preventDefault();

        let formValues = getFormValues(formForCreatingKeywords);
        const requestURL = formForCreatingKeywords.getAttribute('action');

        const requestOptions = new CreateObject({
            method: 'POST',
            url: requestURL,
            headers: {
                'X-CSRFToken': csrftoken,
                'Content-type': 'application/json',
            },
            body: JSON.stringify(formValues),
        });

        sendRequest(requestOptions)
            .catch(err => console.log(err));

        formForCreatingKeywords.reset();

    });
}
