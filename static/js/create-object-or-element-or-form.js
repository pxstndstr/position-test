export function CreateObject(element) {
    for (let key in element) {
        this[key] = element[key]
    }
}

export function CreateElement(object) {
    let element = document.createElement(object.element);

    for (let key in object) {
        if (object[key] === 'submit') {
            element.innerText = 'Применить'
        } else if (key !== 'element') {
            element.setAttribute(key, object[key]);
        }
    }
    return element;
}

export function CreateForm(form, ...objects) {
    let thisElementIsForm = new CreateElement(form);
    for (let i in objects) {
        let element = new CreateElement(objects[i]);
        thisElementIsForm.appendChild(element)
    }
    return thisElementIsForm
}

