import {CreateObject} from "./create-object-or-element-or-form.js";
import {getFormValues} from "./get-form-values.js";
import {sendRequest} from "./send-request.js";
import {getCookie} from './get-cookie.js'


const csrftoken = getCookie('csrftoken');
let $dataOfRequestedCity = $('#id_select_city');
let searchSystemsContainer = document.getElementById('searchSystemsContainer');


$dataOfRequestedCity.autocomplete({
    appendTo: $('.search-results'),
    minLength: 2,
    delay: 500,
    select: getSelectedRegion,

    source: (request, response) => {
        let formAddSearchSystem = document.getElementById('id_form_search_system');
        let formValues = getFormValues(formAddSearchSystem);
        const requestURL = $dataOfRequestedCity.data('url');
        const requestOptions = new CreateObject({
            method: 'POST',
            url: requestURL,
            headers: {
                'X-CSRFToken': csrftoken,
                'Content-type': 'application/json'
            },
            body: JSON.stringify(formValues),

        });

        sendRequest(requestOptions)
            .then(listCities => {
                let results = $.map(listCities, (region) => {
                    return new CreateObject({
                        'label': `${region.title} ${region.subtitle}`,
                        'value': region.region,
                    });
                });

                response(results);

            })
            .catch(err => console.log(err));
    },


}).focus(function () {
    $(this).autocomplete('search', $(this).val())
});


function getSelectedRegion(e, ui) {
    e.preventDefault();
    let completeRegionData = ui.item.label;
    let cityId = ui.item.value;
    $('[name=selected_city_id]').val(cityId);
    $dataOfRequestedCity.val(completeRegionData);
}


$('#id_form_search_system').on('submit', (e) => {
    e.preventDefault();
    let formAddSearchSystem = document.getElementById('id_form_search_system');
    let formValues = getFormValues(formAddSearchSystem);
    const requestURL = formAddSearchSystem.getAttribute('action');
    const requestOptions = new CreateObject({
        method: 'POST',
        url: requestURL,
        headers: {
            'X-CSRFToken': csrftoken,
            'Content-type': 'application/json'
        },
        body: JSON.stringify(formValues),

    });


    sendRequest(requestOptions)
        .then(searchSystems => {
            searchSystemsContainer.innerHTML = '';
            searchSystems.forEach(searchSystem => {
                let li = document.createElement('li');
                // Подумать как лучше это реализовать?
                li.innerText = `${searchSystem.name}, ${searchSystem.city}`;
                searchSystemsContainer.appendChild(li)
            });
        })
        .catch(err => console.log(err));

    // очищаем форму добавления поисковой системы после ее отправки.
    formAddSearchSystem.reset();

});