from django.contrib import admin

from project.models import (
    Keywords,
    KeywordsGroup,
    Project,
    RankingsSite,
    SearchSystem,
)


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'name', 'domain', 'limit_position', 'start_check', 'created', 'updated', 'date_of_check', 'author')
    list_display_links = ('name', 'domain')
    list_filter = ('created', 'limit_position', 'start_check')
    search_fields = ('name', 'domain')
    ordering = ('-id',)
    filter_horizontal = ()

    class Meta:
        model = Project


@admin.register(Keywords)
class KeywordsAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'keywords_group')
    list_display_links = ('name',)
    list_filter = ('name', 'keywords_group')
    search_fields = ('name',)
    ordering = ('-id',)
    filter_horizontal = ()

    class Meta:
        model = Keywords


@admin.register(KeywordsGroup)
class KeywordsGroupAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'project', 'keyword_group_search_system')
    list_display_links = ('name',)
    list_filter = ('name', 'project')
    search_fields = ('name',)
    ordering = ('-id',)
    filter_horizontal = ()

    class Meta:
        model = KeywordsGroup


@admin.register(SearchSystem)
class SearchSystemAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'country', 'city', 'geo_id_city', 'language', 'project')
    list_display_links = ('name',)
    list_filter = ('name', 'project')
    search_fields = ('name',)
    ordering = ('-id',)
    filter_horizontal = ()

    class Meta:
        model = SearchSystem


@admin.register(RankingsSite)
class RankingsSiteAdmin(admin.ModelAdmin):
    list_display = ('id', 'keywords', 'keywords_group', 'position', 'url_site', 'updated', 'created', 'search_system')
    list_display_links = ('keywords',)
    list_filter = ('keywords', 'url_site')
    search_fields = ('keywords', 'keywords_group')
    ordering = ('-id',)
    filter_horizontal = ()

    class Meta:
        model = RankingsSite
