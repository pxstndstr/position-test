from dateutil.relativedelta import relativedelta
from django.db import models
from django.urls import reverse

from account.models import CustomUser


class Project(models.Model):
    LIMIT_HUNDRED = 100
    LIMIT_TWO_HUNDRED = 200

    LIMIT_CHOICES = (
        (LIMIT_HUNDRED, '100'),
        (LIMIT_TWO_HUNDRED, '200'),
    )

    DATE_CHOICES = (
        ('daily', 'Ежедневно'),
        ('every_three_days', 'Раз в 3 дня'),
        ('weekly', 'Раз в неделю'),
        ('monthly', 'Раз в месяц'),
        ('on_request', 'По запросу'),
    )

    name = models.CharField('Название проекта', max_length=255)

    slug = models.SlugField(max_length=50, unique=True, default="1", verbose_name="Символьный код проекта")

    domain = models.URLField('Адрес сайта', max_length=255)

    limit_position = models.IntegerField(
        'Глубина сбора позиций',
        choices=LIMIT_CHOICES,
        default=LIMIT_HUNDRED,
        help_text='Сервис будет искать сайт включительно до 100-й или 200-й позиции'
    )

    start_check = models.CharField(
        'Периодичность проверок',
        max_length=30,
        choices=DATE_CHOICES,
        default='weekly',
    )

    created = models.DateTimeField('Дата добавления', auto_now_add=True)

    updated = models.DateTimeField('Дата изменения', auto_now=True)

    date_of_check = models.DateTimeField('Дата проверки', blank=True, null=True)

    author = models.ForeignKey(
        CustomUser,
        verbose_name='Автор проекта',
        on_delete=models.CASCADE
    )

    def get_next_check_date(self, date_of_check):
        if self.start_check == 'daily':
            return date_of_check + relativedelta(days=1)

        elif self.start_check == 'every_three_days':
            return date_of_check + relativedelta(days=3)

        elif self.start_check == 'weekly':
            return date_of_check + relativedelta(weeks=1)

        elif self.start_check == 'monthly':
            return date_of_check + relativedelta(month=1)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'
        ordering = ('-id',)


class KeywordsGroup(models.Model):
    name = models.CharField('Название группы', max_length=255)

    project = models.ForeignKey(
        Project,
        verbose_name='Проект',
        on_delete=models.CASCADE
    )

    search_systems = models.ManyToManyField(
        'SearchSystem',
        blank=True
    )

    def keyword_group_search_system(self):
        return [search_system for search_system in self.search_systems.all()]

    keyword_group_search_system.short_description = 'Поисковые системы группы ключевых слов'

    def __str__(self):
        return self.name

    # def get_absolute_url(self):
    #     return reverse('project:keywords_group_detail', kwargs={'pk': self.id})

    class Meta:
        verbose_name = 'Группа ключевых слов'
        verbose_name_plural = 'Группы ключевых слов'


class Keywords(models.Model):
    name = models.TextField('Перечень слов', max_length=255)

    keywords_group = models.ForeignKey(
        KeywordsGroup,
        verbose_name='Группа ключевых слов',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Слово'
        verbose_name_plural = 'Слова'


class SearchSystem(models.Model):
    YANDEX = 'yandex'
    GOOGLE = 'google'
    ENGLISH = 'en'
    RUSSIAN = 'ru'

    LANGUAGE_CHOICES = (
        (ENGLISH, 'english'),
        (RUSSIAN, 'русский'),
    )

    SEARCH_SYSTEM_CHOICES = (
        (YANDEX, 'яндекс'),
        (GOOGLE, 'google'),
    )

    name = models.CharField(
        'Поисковая система',
        max_length=25,
        choices=SEARCH_SYSTEM_CHOICES,
        default=YANDEX
    )

    country = models.CharField('Страна', max_length=50)

    city = models.CharField('Регион', max_length=100)

    geo_id_city = models.IntegerField('Geo id города', )

    language = models.CharField(
        'Язык',
        max_length=25,
        choices=LANGUAGE_CHOICES,
        default=RUSSIAN
    )

    project = models.ForeignKey(
        Project,
        verbose_name='Проект',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f'{self.name}, {self.city}'

    class Meta:
        verbose_name = 'Поисковая система'
        verbose_name_plural = 'Поисковые системы'


class RankingsSite(models.Model):
    position = models.PositiveSmallIntegerField('Позиция сайта', )
    project = models.ForeignKey(
        Project,
        verbose_name='Проект',
        on_delete=models.CASCADE
    )
    url_site = models.URLField('Адрес сайта', max_length=255)

    keywords = models.CharField('Ключевые слова', max_length=150)

    keywords_group = models.ForeignKey(
        KeywordsGroup,
        verbose_name='Группы ключевых слов',
        on_delete=models.CASCADE,
    )
    created = models.DateTimeField('Дата добавления', auto_now_add=True)
    updated = models.DateTimeField('Дата изменения', auto_now=True)
    search_system = models.ForeignKey(
        SearchSystem,
        verbose_name='Поисковая система',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        # Продумать в каком виде лучше отображать записи данной модели??
        return f'''
            ПС: {self.search_system},
            фраза: {self.keywords}, 
            URL: {self.url_site}, 
            позиция: {self.position}, 
            дата: {self.updated}   
                '''

    class Meta:
        verbose_name = 'Позиция сайта'
        verbose_name_plural = 'Позиции сайта'
