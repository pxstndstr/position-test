from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.generic import DetailView, ListView, View
from django.views.generic.edit import (
    CreateView,
    DeleteView,
    UpdateView,
)

from project.forms import (
    AddProjectForm,
    AddKeywordsForm,
    AddSearchSystemForm,
    ChangeProjectForm,
)

from project.models import (
    KeywordsGroup,
    Project,
    SearchSystem,
    Keywords,
    RankingsSite
)
from services.services_of_project import (
    service_adding_keywords_to_keyword_group,
    service_adding_keyword_groups,
    SearchSystemService,
    ProjectService,
)

def add_keywords_group(request, project_id):
    if request.method == 'POST':
        keywords_group_id = service_adding_keyword_groups(request, project_id)
        return JsonResponse(keywords_group_id)


def get_cities_to_choose(request):
    cities_to_choose = SearchSystemService.get_cities_to_choose(request)
    return HttpResponse(cities_to_choose)


class AddProjectView(LoginRequiredMixin, CreateView):
    template_name = 'project/create.html'
    form_class = AddProjectForm

    def form_valid(self, form):
        project = form.save(commit=False)
        project.author = self.request.user
        project.save()
        return HttpResponseRedirect(reverse_lazy('project:keywords_create', kwargs={'project_id': project.id}))


class ProjectListView(LoginRequiredMixin, ListView):
    template_name = 'project/profile.html'
    context_object_name = 'project_list'

    def get_queryset(self):
        return Project.objects.filter(author_id=self.request.user.pk)


class DetailProjectView(DetailView):
    model = Project
    template_name = 'project/detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['search_systems'] = SearchSystem.objects.filter(project__slug=self.kwargs['slug'])
        return context


class DeleteProjectView(LoginRequiredMixin, DeleteView):
    model = Project
    template_name = 'project/delete.html'
    success_url = reverse_lazy('account:index')


class ChangeProjectView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Project
    template_name = 'project/change.html'
    form_class = ChangeProjectForm
    success_message = 'Настройки проекта изменены'

    def get_success_url(self):
        return reverse_lazy('project:project_detail', kwargs={'slug': self.kwargs['slug']})


class DetailKeywordsGroupView(DetailView):
    model = KeywordsGroup
    template_name = 'keywords_group/detail.html'


class AddKeywordsView(LoginRequiredMixin, CreateView):
    template_name = 'keywords/create.html'
    form_class = AddKeywordsForm

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['url'] = reverse_lazy('project:create_keywords_group',
                                      kwargs={'project_id': self.kwargs['project_id']})

        return context

    def get_initial(self):
        return {'project_id': self.kwargs['project_id']}

    def post(self, request, *args, **kwargs):
        service_adding_keywords_to_keyword_group(request)
        return JsonResponse({'status': 'success'})


class AddSearchSystemView(LoginRequiredMixin, CreateView):
    template_name = 'search_system/create.html'
    form_class = AddSearchSystemForm

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['url'] = reverse_lazy('project:get_cities_to_choose')
        context['search_systems'] = SearchSystem.objects.filter(project_id=self.kwargs['project_id'])
        return context

    def post(self, request, *args, **kwargs):
        search_system = SearchSystemService()
        search_system.add_search_system(request, **kwargs)
        search_systems_of_project = list(
            SearchSystem.objects.filter(project=self.kwargs['project_id']).values('name', 'city'))
        return JsonResponse(search_systems_of_project, safe=False)  # На сколько безопасно использовать safe=False ?


class DetailSearchSystemsView(LoginRequiredMixin, DetailView):
    model = SearchSystem
    template_name = 'search_system/detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['keywords_groups'] = KeywordsGroup.objects.filter(search_systems=self.kwargs['pk'])
        return context


class PositionCheckView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        project_service = ProjectService()
        project_service.start_checking_projects()
        return HttpResponse('Good Nice')

