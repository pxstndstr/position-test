# Generated by Django 3.1 on 2021-02-09 17:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0002_project_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='keywords',
            name='name',
            field=models.TextField(max_length=255, verbose_name='Перечень слов'),
        ),
    ]
