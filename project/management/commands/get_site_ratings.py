from django.core.management.base import BaseCommand
from services.services_of_project import ProjectService


class Command(BaseCommand):
    help = 'Выводит рейтинги сайта'

    def handle(self, *args, **options):
        project_service = ProjectService()
        project_service.start_checking_projects()
