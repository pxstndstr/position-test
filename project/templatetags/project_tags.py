from django import template

from project.models import (
    KeywordsGroup,
    Project,
    SearchSystem,
)

register = template.Library()


@register.inclusion_tag('sidebar.html', takes_context=True)
def get_projects(context, user):
    projects = Project.objects.filter(author=user)
    return {'my_projects': projects}

