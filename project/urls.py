from django.urls import path

from project.views import (
    add_keywords_group,
    get_cities_to_choose,
    AddKeywordsView,
    AddProjectView,
    AddSearchSystemView,
    ChangeProjectView,
    DetailKeywordsGroupView,
    DetailProjectView,
    DetailSearchSystemsView,
    DeleteProjectView,
    ProjectListView,
    PositionCheckView,
)

app_name = 'project'
urlpatterns = [
    path('all/check/', PositionCheckView.as_view(), name='check_all_positions'),
    path('keywords/add/<int:project_id>/', AddKeywordsView.as_view(), name='keywords_create'),
    path('keywords_group/detail/<int:pk>/', DetailKeywordsGroupView.as_view(), name='keywords_group_detail'),
    path('add/', AddProjectView.as_view(), name='project_create'),
    path('', ProjectListView.as_view(), name='project_profile'),
    path('<slug:slug>/update/', ChangeProjectView.as_view(), name='project_change'),
    path('<slug:slug>/delete/', DeleteProjectView.as_view(), name='project_delete'),
    path('<slug:slug>/', DetailProjectView.as_view(), name='project_detail'),
    path('search_system/add/<int:project_id>/', AddSearchSystemView.as_view(), name='search_system_create'),
    path('search_system/<int:pk>/', DetailSearchSystemsView.as_view(), name='search_system_detail'),
    path('ajax/get_cities_to_choose/', get_cities_to_choose, name='get_cities_to_choose'),
    path('ajax/create_keywords_group/<int:project_id>/', add_keywords_group, name='create_keywords_group'),
]
