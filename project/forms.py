from django import forms

from project.models import (
    Keywords,
    KeywordsGroup,
    Project,
    SearchSystem
)


class AddProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('name', 'domain', 'limit_position', 'start_check')


class ChangeProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('name', 'domain', 'limit_position', 'start_check')


class AddKeywordsGroupForm(forms.ModelForm):
    name = forms.CharField(
        label='Название группы',
        max_length=255
    )

    class Meta:
        model = KeywordsGroup
        fields = ('name',)


class AddKeywordsForm(forms.ModelForm):
    name = forms.CharField(
        label='Добавить слово',
        widget=forms.widgets.Textarea(),
    )

    class Meta:
        model = Keywords
        fields = ('name', 'keywords_group')

    def __init__(self, *args, **kwargs):
        super(AddKeywordsForm, self).__init__(*args, **kwargs)
        initial = kwargs.get('initial')
        self.fields['keywords_group'].queryset = KeywordsGroup.objects.filter(project_id=initial['project_id'])


class AddSearchSystemForm(forms.ModelForm):
    class Meta:
        model = SearchSystem
        fields = ('name', 'country', 'city', 'language')
